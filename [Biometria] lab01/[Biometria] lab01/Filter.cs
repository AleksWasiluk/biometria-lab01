﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Biometria__lab01
{
    public delegate void PixelFunction(int x, int y, DirectBitmap bitmap);
    public delegate void Algorithm(DirectBitmap bitmap);

    public class Filter
    {
        private DirectBitmap directBitmap;
        private PixelFunction PixelFunction;
        private PixelFunction PostPixelFunction;

        private Algorithm algorithm;

        enum TypeCompute { ImageAlgorithm, PixelAlgorithm }

        TypeCompute typeCompute;

        private double scalePostProcesing;

        public Filter(DirectBitmap directBitmap, PixelFunction pixelFunction, PixelFunction postPixelFunction = null)
        {
            this.PostPixelFunction = postPixelFunction;
            this.directBitmap = directBitmap;
            this.PixelFunction = pixelFunction;

            typeCompute = TypeCompute.PixelAlgorithm;
        }

        public Filter(DirectBitmap directBitmap, Algorithm algorithm)
        {
            this.directBitmap = directBitmap;

            this.algorithm = algorithm;

            typeCompute = TypeCompute.ImageAlgorithm;

        }

        public void Run()
        {
            for(int width = 0; width < directBitmap.Width; width++)
            {
                for(int height=0; height < directBitmap.Height; height++)
                {
                    PixelFunction(width, height, directBitmap);
                }
            }

            if(PostPixelFunction != null)
            {
                for (int width = 0; width < directBitmap.Width; width++)
                {
                    for (int height = 0; height < directBitmap.Height; height++)
                    {
                        PostPixelFunction(width, height, directBitmap);
                    }
                }
            }

            directBitmap.MaxColorIsComputed = false;
            directBitmap.MinColorIsComputed = false;
        }

        public void RunAlgorithm()
        {
            algorithm(directBitmap);
        }
    }
}
