﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace _Biometria__lab01
{
    public class DirectBitmap : IDisposable
    {
        public Bitmap Bitmap { get; private set; }
        public Int32[] Bits { get; private set; }
        public bool Disposed { get; private set; }
        public int Height { get; private set; }
        public int Width { get; private set; }

        private Color maxColor = Color.Empty;
        private Color minColor = Color.Empty;

        public bool MaxColorIsComputed { get; set; }

        internal bool HasPixel(int w, int h)
        {
            if( w > -1 && w < Width && h > -1 && h < Height)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool MinColorIsComputed { get; set; }

        public Color MaxColor
        {
            get
            {
                if(!MaxColorIsComputed)
                {
                    MaxColorIsComputed = true;

                    var tempMaxColor = new byte[3];

                    for (int i = 0; i < tempMaxColor.Length; i++)
                    {
                        tempMaxColor[i] = byte.MinValue;
                    }

                    for (int w = 0; w < Width; w++)
                    {
                        for( int h=0; h< Height; h++)
                        {
                            Color c = GetPixel(w, h);
                            tempMaxColor[0] = Math.Max(tempMaxColor[0], c.R);                            
                            tempMaxColor[1] = Math.Max(tempMaxColor[0], c.G);                            
                            tempMaxColor[2] = Math.Max(tempMaxColor[0], c.B);                            
                        }
                    }

                    maxColor = Color.FromArgb(tempMaxColor[0], tempMaxColor[1], tempMaxColor[2]);
                }

                return maxColor;
            }
        }

        public Color MinColor
        {
            get
            {
                if (!MinColorIsComputed)
                {
                    MinColorIsComputed = true;

                    var tempMinColor = new byte[3];

                    for(int i=0; i < tempMinColor.Length; i++)
                    {
                        tempMinColor[i] = byte.MaxValue;
                    }

                    for (int w = 0; w < Width; w++)
                    {
                        for (int h = 0; h < Height; h++)
                        {
                            Color c = GetPixel(w, h);
                            tempMinColor[0] = Math.Min(tempMinColor[0], c.R);
                            tempMinColor[1] = Math.Min(tempMinColor[0], c.G);
                            tempMinColor[2] = Math.Min(tempMinColor[0], c.B);
                        }
                    }

                    minColor = Color.FromArgb(tempMinColor[0], tempMinColor[1], tempMinColor[2]);
                }

                return minColor;
            }
        }
        protected GCHandle BitsHandle { get; private set; }

        public DirectBitmap(int width, int height)
        {
            Width = width;
            Height = height;
            Bits = new Int32[width * height];
            BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
            Bitmap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());

        }

        public DirectBitmap(Bitmap bitmap)
        {
            Width = bitmap.Width;
            Height = bitmap.Height;
            BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppPArgb);

            
            Bits = new Int32[Width * Height];
            BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);

            Marshal.Copy(bitmapData.Scan0, Bits, 0, Width * Height);

            Bitmap = new Bitmap(Width, Height, Width * 4, PixelFormat.Format32bppPArgb, BitsHandle.AddrOfPinnedObject());

            bitmap.UnlockBits(bitmapData);
        }

        public void SetPixel(int x, int y, Color colour)
        {
            int index = x + (y * Width);
            int col = colour.ToArgb();

            Bits[index] = col;
        }

        public Color GetPixel(int x, int y)
        {
            int index = x + (y * Width);
            int col = Bits[index];
            Color result = Color.FromArgb(col);

            return result;
        }

        public void Dispose()
        {
            if (Disposed) return;
            Disposed = true;
            Bitmap.Dispose();
            BitsHandle.Free();
        }
    }
}
