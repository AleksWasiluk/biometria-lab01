﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _Biometria__lab01
{
    class MyHistogram
    {
        private const int nColorInPixel = 3; // r, g,b so 3
        private const int nBuckets = Byte.MaxValue + 1;

        DirectBitmap imageBitmap;
        PictureBox[] pictureBoxes;

        DirectBitmap[] histogramBitmaps;
        int[,] pixelBrightness = new int[nColorInPixel, nBuckets];

        int nMaxPixelBrightness;


        public MyHistogram(DirectBitmap ImageBitmap, PictureBox [] pictureBoxes)
        {
            imageBitmap = ImageBitmap;
            this.pictureBoxes = pictureBoxes;

            histogramBitmaps = new DirectBitmap[pictureBoxes.Length];
            for(int i=0; i< pictureBoxes.Length; i++)
            {
                histogramBitmaps[i] = new DirectBitmap(pictureBoxes[i].Width, pictureBoxes[i].Height);
            }
        }

        public void Show()
        {
            ComputeBuckets();

            RenderGraphs();
        }

        private void RenderGraphs()
        {
            for(int i = 0 ; i < nColorInPixel ; i++)
            {
                RenderGraph(i);
            }
        }

        private void RenderGraph(int i)
        {
            const int widthColumn = 1;

            for(int bucket =0;bucket < nBuckets; bucket++)
            {
                int heightColumn =(int)( ((double) pixelBrightness[i, bucket] / nMaxPixelBrightness) * histogramBitmaps[i].Height);

                DrawColumnInGraph(widthColumn, heightColumn,bucket, i);
            }
        }

        private void DrawColumnInGraph(int widthColumn, int heightColumn,int nbucket, int i)
        {
            Color c = GetColorGraph(i);

            for(int w = 0; w < widthColumn; w++)
            {
                for(int h= histogramBitmaps[i].Height - heightColumn; h< histogramBitmaps[i].Height; h++)
                {
                    histogramBitmaps[i].SetPixel(w + nbucket*widthColumn, h, c);
                }
            }

            pictureBoxes[i].Image = histogramBitmaps[i].Bitmap;
        }

        private Color GetColorGraph(int i)
        {
            Color c = Color.Empty;

            switch (i)
            {
                case 0:
                    {
                        c = Color.Red;
                        break;
                    }
                case 1:
                    {
                        c = Color.Green;
                        break;
                    }
                case 2:
                    {
                        c = Color.Blue;
                        break;
                    }
            }

            return c;
        }

        private void ComputeBuckets()
        {
            for(int w= 0; w< imageBitmap.Width;w++)
            {
                for(int h=0; h<imageBitmap.Height; h++)
                {
                    Color c = imageBitmap.GetPixel(w, h);

                    pixelBrightness[0, c.R]++;
                    pixelBrightness[1, c.G]++;
                    pixelBrightness[2, c.B]++;
                }
            }

            //compute max
            for(int i = 0; i < nBuckets; i++)
            {
                for(int j = 0; j < nColorInPixel; j++)
                {
                    nMaxPixelBrightness = Math.Max( pixelBrightness[j, i], nMaxPixelBrightness );
                }
            }
        }
       
    }
}
