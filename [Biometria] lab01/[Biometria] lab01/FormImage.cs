﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace _Biometria__lab01
{
    public partial class FormImage : Form
    {
        DirectBitmap imageBitmap;

        public FormImage()
        {
            InitializeComponent();
        }

        private void LoadPictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files(*.jpg, *.jpeg, *.jpe, *.jfif, *.png, *bmp) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png; *.bmp";

                DialogResult dialogResult = dlg.ShowDialog();

                if (dialogResult == DialogResult.OK)
                {
                    imageBitmap?.Dispose();
                    // Create a new Bitmap object from the picture file on disk,
                    // and assign that to the PictureBox.Image property
                    imageBitmap = new DirectBitmap(new Bitmap(dlg.FileName));

                    pictureBox1.Image = imageBitmap.Bitmap;
                }
                else
                {
                    MessageBox.Show("Nie wybrano zadnego zdjecia");
                }
            }

        }

        private void negationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap,
            pixelFunction: (x, y, bitmap) =>
            {
                Color color = bitmap.GetPixel(x, y);

                bitmap.SetPixel(x, y, Color.FromArgb(255 - color.R, 255 - color.G, 255 - color.B));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void greyscaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap,
            pixelFunction: (x, y, bitmap) =>
            {
                Color color = bitmap.GetPixel(x, y);

                int avg = (color.R + color.G + color.B) / 3;

                bitmap.SetPixel(x, y, Color.FromArgb(avg, avg, avg));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void normalizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap, 
            pixelFunction:   (x, y, bitmap) => 
            {
                Color color = bitmap.GetPixel(x, y);

                Color minColor = bitmap.MinColor;
                Color maxColor = bitmap.MaxColor;

                byte r = (byte)((double) 255 * (color.R - minColor.R)/(maxColor.R - minColor.R) );
                byte g = (byte)((double) 255 * (color.G - minColor.G)/(maxColor.G - minColor.G) );
                byte b = (byte)((double) 255 * (color.B - minColor.B)/(maxColor.B - minColor.B) );

                bitmap.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void contrastEditingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap,
            pixelFunction: (x, y, bitmap) =>
            {
                Color c = bitmap.GetPixel(x, y);

                double a = 1.2;
                Func<double, double, double> func;

                byte edgeValue;

                if(a >= 1)
                {
                    func = Math.Min;
                    edgeValue = byte.MaxValue;
                }
                else
                {
                    func = Math.Max;
                    edgeValue = byte.MinValue;
                }

                byte r = (byte)func((a * c.R), edgeValue);
                byte g = (byte)func((a * c.G), edgeValue);
                byte b = (byte)func((a * c.B), edgeValue);

                bitmap.SetPixel(x, y, Color.FromArgb(r,g,b));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void brighteningToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap,
            pixelFunction: (x, y, bitmap) =>
            {
                Color c = bitmap.GetPixel(x, y);

                double a = 50;

                Func<double, double, double> func;

                int edgeValue;

                if (a >= 0)
                {
                    func = Math.Min;
                    edgeValue = byte.MaxValue;
                }
                else
                {
                    func = Math.Max;
                    edgeValue = byte.MinValue;
                }

                byte r = (byte)func((a + c.R), edgeValue);
                byte g = (byte)func((a + c.G), edgeValue);
                byte b = (byte)func((a + c.B), edgeValue);

                bitmap.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void contrastWithNormalizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap,
            pixelFunction: (x, y, bitmap) =>
            {
                Color c = bitmap.GetPixel(x, y);

                Color cMax = bitmap.MaxColor;

                double alpha = 1.5;

                byte r = (byte) (255 * Math.Pow( (double) c.R / cMax.R , alpha));
                byte g = (byte) (255 * Math.Pow( (double) c.G / cMax.G , alpha));
                byte b = (byte) (255 * Math.Pow( (double) c.B / cMax.B , alpha));

                bitmap.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void logarithmWithNormalizationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap,
            pixelFunction: (x, y, bitmap) =>
            {
                Color c = bitmap.GetPixel(x, y);

                Color cMax = bitmap.MaxColor;

                byte r = (byte)(255 * Math.Log( 1 + (double) c.R ) / Math.Log( 1 + cMax.R) );
                byte g = (byte)(255 * Math.Log( 1 + (double) c.G ) / Math.Log( 1 + cMax.G) );
                byte b = (byte)(255 * Math.Log( 1 + (double) c.B ) / Math.Log( 1 + cMax.B) );

                bitmap.SetPixel(x, y, Color.FromArgb(r, g, b));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void thresholdToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filter = new Filter(imageBitmap,
            pixelFunction: (x, y, bitmap) =>
            {
                Color c = bitmap.GetPixel(x, y);

                int a = 50;

                int outColor = (c.R + c.G + c.B) / 3 > a ? 255 : 0;

                bitmap.SetPixel(x, y, Color.FromArgb(outColor, outColor, outColor));
            }
            );

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void showHistogramToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new FormHistogram(imageBitmap);

            form.Show();
        }

        private void simpleBlurToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] fields = 
            { 
                { 1, 1, 1 }, 
                { 1, 1, 1 }, 
                { 1, 1, 1 }
            };

            var filter = new MatrixFilter(imageBitmap, new MyMatrix(fields));

            filter.Run();

            pictureBox1.Image = imageBitmap.Bitmap;

        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            double[,] fields =
            {
                { 1, 2, 1 },
                { 0, 0, 0 },
                { -1, -2, -1 }
            };

            var filter = new MyMatrixAlgorithmWithNegativeValues(imageBitmap, new MyMatrix(fields));

            filter.DoAlgorithm();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void mixed0With90ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] fields1 =
            {
                { -1, 0, 1 },
                { -2, 0, 2 },
                { -1, 0, 1 }
            };

            double[,] fields2 =
            {
                { 1, 2, 1 },
                { 0, 0, 0 },
                { -1, -2, -1 }
            };

            var filter = new DoubleMatrixFilter(imageBitmap, new MyMatrix(fields1), new MyMatrix(fields2));

            filter.RunAlgorithm();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            double[,] fields =
            {
                { -1, 0, 1 },
                { -2, 0, 2 },
                { -1, 0, 1 }
            };

            var filter = new MyMatrixAlgorithmWithNegativeValues(imageBitmap, new MyMatrix(fields));

            filter.DoAlgorithm();

            pictureBox1.Image = imageBitmap.Bitmap;
        }

        private void simple141ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double[,] fields =
            {
                { 0, -1, 0 },
                { -1, 4, -1 },
                { 0, -1, 0 }
            };

            var filter = new MyMatrixAlgorithmWithNegativeValues(imageBitmap, new MyMatrix(fields));

            filter.DoAlgorithm();

            pictureBox1.Image = imageBitmap.Bitmap;
        }
    }
}
