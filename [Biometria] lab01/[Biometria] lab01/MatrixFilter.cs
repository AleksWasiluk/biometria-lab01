﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Biometria__lab01
{
    class MatrixFilter : Filter
    {
        public MatrixFilter(DirectBitmap directBitmap, MyMatrix m) 
            : base(directBitmap,
                  pixelFunction: (x,y,bmp) => 
                  {
                      double r = 0, g = 0, b = 0;

                      int size = 0;
                      for (int w = x - m.Dimension/2 , matrixW = 0; w <= x + m.Dimension/2; w++, matrixW++)
                      {
                          for (int h = y - m.Dimension/2, matrixH = 0; h <= y + m.Dimension/2; h++, matrixH++)
                          {
                              if(directBitmap.HasPixel(w,h))
                              {
                                  Color c = directBitmap.GetPixel(w, h);

                                  double fieldMatrix = m[matrixW, matrixH];

                                 
                                  r += c.R;
                                  g += c.G;
                                  b += c.B;
                                  

                                  size = size + (int) Math.Abs( m[matrixW, matrixH] );
                              }
                          }
                      }

                      r = Math.Min( Math.Abs(r / size), 255);
                      g = Math.Min( Math.Abs(g / size), 255);
                      b = Math.Min( Math.Abs(b / size), 255);

                      directBitmap.SetPixel(x, y, Color.FromArgb((int)r,(int) g,(int) b));
                  })
        {
        }
    }

    class MatrixFilterWithMyNormalization : Filter
    {
        public MatrixFilterWithMyNormalization(DirectBitmap directBitmap, MyMatrix m)
            : base(directBitmap,
                  pixelFunction: (x, y, bmp) =>
                  {
                      double r = 0, g = 0, b = 0;

                      int size = 0;
                      for (int w = x - m.Dimension / 2, matrixW = 0; w <= x + m.Dimension / 2; w++, matrixW++)
                      {
                          for (int h = y - m.Dimension / 2, matrixH = 0; h <= y + m.Dimension / 2; h++, matrixH++)
                          {
                              if (directBitmap.HasPixel(w, h))
                              {
                                  Color c = directBitmap.GetPixel(w, h);

                                  double fieldMatrix = m[matrixW, matrixH];

                                  if(fieldMatrix <0)
                                  {
                                      r += 255 - c.R;
                                      g += 255 - c.G;
                                      b += 255 - c.B;
                                  }
                                  else
                                  {
                                    r += c.R;
                                    g += c.G;
                                    b += c.B;
                                  }



                                  size = size + (int)Math.Abs(m[matrixW, matrixH]);
                              }
                          }
                      }

                      r = Math.Min(Math.Abs(r / size), 255);
                      g = Math.Min(Math.Abs(g / size), 255);
                      b = Math.Min(Math.Abs(b / size), 255);

                      directBitmap.SetPixel(x, y, Color.FromArgb((int)r, (int)g, (int)b));
                  })
        {
        }
    }
    class DoubleMatrixFilter : Filter
    {
        public DoubleMatrixFilter(DirectBitmap directBitmap, MyMatrix m1, MyMatrix m2)
            : base(directBitmap,
                  algorithm: (bmp) =>
                  {
                      //if m1.dim != m2.dim throw exception

                      int dimension = m1.Dimension;

                      int[,,] IntBitMap = new int [3, directBitmap.Width, directBitmap.Height];

                      MyColor maxColors = new MyColor();

                      for (int x = 0; x < directBitmap.Width; x++)
                      {
                          for (int y = 0; y < directBitmap.Height; y++)
                          {
                              var colorX = new MyColor();
                              var colorY = new MyColor();

                              for (int w = x - dimension / 2, matrixW = 0; w <= x + dimension / 2; w++, matrixW++)
                              {

                                  for (int h = y - dimension / 2, matrixH = 0; h <= y + dimension / 2; h++, matrixH++)
                                  {
                                      if (directBitmap.HasPixel(w, h))
                                      {
                                          Color c = directBitmap.GetPixel(w, h);

                                          double fieldMatrixM1 = m1[matrixW, matrixH];
                                          double fieldMatrixM2 = m2[matrixW, matrixH];

                                          colorX.R += (int)(c.R * fieldMatrixM1);
                                          colorX.G += (int)(c.G * fieldMatrixM1);
                                          colorX.B += (int)(c.B * fieldMatrixM1);

                                          colorY.R += (int)(c.R * fieldMatrixM2);
                                          colorY.G += (int)(c.G * fieldMatrixM2);
                                          colorY.B += (int)(c.B * fieldMatrixM2);
                                      }
                                  }
                              }

                              colorX.R = (int) Math.Sqrt(colorX.R * colorX.R + colorY.R * colorY.R);
                              colorX.G = (int) Math.Sqrt(colorX.G * colorX.G + colorY.G * colorY.G);
                              colorX.B = (int) Math.Sqrt(colorX.B * colorX.B + colorY.B * colorY.B);

                              maxColors.R = Math.Max(colorX.R, maxColors.R);
                              maxColors.G = Math.Max(colorX.G, maxColors.G);
                              maxColors.B = Math.Max(colorX.B, maxColors.B);

                              IntBitMap[0, x, y] = colorX.R;
                              IntBitMap[1, x, y] = colorX.G;
                              IntBitMap[2, x, y] = colorX.B;
                          }
                      }

                      for (int x = 0; x < directBitmap.Width; x++)
                      {
                          for (int y = 0; y < directBitmap.Height; y++)
                          {
                              double r = IntBitMap[0, x, y] *  255 / maxColors.R;
                              double g = IntBitMap[1, x, y] * 255 / maxColors.G;
                              double b = IntBitMap[2, x, y] * 255 / maxColors.B;

                              directBitmap.SetPixel(x, y, Color.FromArgb((int) r,(int) g,(int) b));
                          }
                      }
                  })
        {
        }
    }
}
