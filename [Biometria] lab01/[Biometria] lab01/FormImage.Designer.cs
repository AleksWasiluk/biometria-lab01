﻿namespace _Biometria__lab01
{
    partial class FormImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.loadPictureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.linearOperationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greyscaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contrastEditingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.brighteningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nonLinearOperationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contrastWithNormalizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logarithmWithNormalizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showHistogramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.convulutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simpleBlurToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobelOperatorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.mixed0With90ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.resetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setGreyScaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laplaciansToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simple141ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 422);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadPictureToolStripMenuItem,
            this.loadFilterToolStripMenuItem,
            this.resetToolStripMenuItem,
            this.setGreyScaleToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // loadPictureToolStripMenuItem
            // 
            this.loadPictureToolStripMenuItem.Name = "loadPictureToolStripMenuItem";
            this.loadPictureToolStripMenuItem.Size = new System.Drawing.Size(103, 24);
            this.loadPictureToolStripMenuItem.Text = "Load Picture";
            this.loadPictureToolStripMenuItem.Click += new System.EventHandler(this.LoadPictureToolStripMenuItem_Click);
            // 
            // loadFilterToolStripMenuItem
            // 
            this.loadFilterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.linearOperationsToolStripMenuItem,
            this.nonLinearOperationsToolStripMenuItem,
            this.thresholdToolStripMenuItem,
            this.showHistogramToolStripMenuItem,
            this.convulutionToolStripMenuItem});
            this.loadFilterToolStripMenuItem.Name = "loadFilterToolStripMenuItem";
            this.loadFilterToolStripMenuItem.Size = new System.Drawing.Size(91, 24);
            this.loadFilterToolStripMenuItem.Text = "Load Filter";
            // 
            // linearOperationsToolStripMenuItem
            // 
            this.linearOperationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.negationToolStripMenuItem,
            this.greyscaleToolStripMenuItem,
            this.normalizationToolStripMenuItem,
            this.contrastEditingToolStripMenuItem,
            this.brighteningToolStripMenuItem});
            this.linearOperationsToolStripMenuItem.Name = "linearOperationsToolStripMenuItem";
            this.linearOperationsToolStripMenuItem.Size = new System.Drawing.Size(240, 26);
            this.linearOperationsToolStripMenuItem.Text = "Linear Operations";
            // 
            // negationToolStripMenuItem
            // 
            this.negationToolStripMenuItem.Name = "negationToolStripMenuItem";
            this.negationToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.negationToolStripMenuItem.Text = "Negation";
            this.negationToolStripMenuItem.Click += new System.EventHandler(this.negationToolStripMenuItem_Click);
            // 
            // greyscaleToolStripMenuItem
            // 
            this.greyscaleToolStripMenuItem.Name = "greyscaleToolStripMenuItem";
            this.greyscaleToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.greyscaleToolStripMenuItem.Text = "Greyscale";
            this.greyscaleToolStripMenuItem.Click += new System.EventHandler(this.greyscaleToolStripMenuItem_Click);
            // 
            // normalizationToolStripMenuItem
            // 
            this.normalizationToolStripMenuItem.Name = "normalizationToolStripMenuItem";
            this.normalizationToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.normalizationToolStripMenuItem.Text = "Normalization";
            this.normalizationToolStripMenuItem.Click += new System.EventHandler(this.normalizationToolStripMenuItem_Click);
            // 
            // contrastEditingToolStripMenuItem
            // 
            this.contrastEditingToolStripMenuItem.Name = "contrastEditingToolStripMenuItem";
            this.contrastEditingToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.contrastEditingToolStripMenuItem.Text = "Contrast Editing";
            this.contrastEditingToolStripMenuItem.Click += new System.EventHandler(this.contrastEditingToolStripMenuItem_Click);
            // 
            // brighteningToolStripMenuItem
            // 
            this.brighteningToolStripMenuItem.Name = "brighteningToolStripMenuItem";
            this.brighteningToolStripMenuItem.Size = new System.Drawing.Size(235, 26);
            this.brighteningToolStripMenuItem.Text = "Brightening/Darkening";
            this.brighteningToolStripMenuItem.Click += new System.EventHandler(this.brighteningToolStripMenuItem_Click);
            // 
            // nonLinearOperationsToolStripMenuItem
            // 
            this.nonLinearOperationsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contrastWithNormalizationToolStripMenuItem,
            this.logarithmWithNormalizationToolStripMenuItem});
            this.nonLinearOperationsToolStripMenuItem.Name = "nonLinearOperationsToolStripMenuItem";
            this.nonLinearOperationsToolStripMenuItem.Size = new System.Drawing.Size(240, 26);
            this.nonLinearOperationsToolStripMenuItem.Text = "Non Linear Operations";
            // 
            // contrastWithNormalizationToolStripMenuItem
            // 
            this.contrastWithNormalizationToolStripMenuItem.Name = "contrastWithNormalizationToolStripMenuItem";
            this.contrastWithNormalizationToolStripMenuItem.Size = new System.Drawing.Size(286, 26);
            this.contrastWithNormalizationToolStripMenuItem.Text = "Contrast With Normalization";
            this.contrastWithNormalizationToolStripMenuItem.Click += new System.EventHandler(this.contrastWithNormalizationToolStripMenuItem_Click);
            // 
            // logarithmWithNormalizationToolStripMenuItem
            // 
            this.logarithmWithNormalizationToolStripMenuItem.Name = "logarithmWithNormalizationToolStripMenuItem";
            this.logarithmWithNormalizationToolStripMenuItem.Size = new System.Drawing.Size(286, 26);
            this.logarithmWithNormalizationToolStripMenuItem.Text = "Logarithm With Normalization";
            this.logarithmWithNormalizationToolStripMenuItem.Click += new System.EventHandler(this.logarithmWithNormalizationToolStripMenuItem_Click);
            // 
            // thresholdToolStripMenuItem
            // 
            this.thresholdToolStripMenuItem.Name = "thresholdToolStripMenuItem";
            this.thresholdToolStripMenuItem.Size = new System.Drawing.Size(240, 26);
            this.thresholdToolStripMenuItem.Text = "Thresholding";
            this.thresholdToolStripMenuItem.Click += new System.EventHandler(this.thresholdToolStripMenuItem_Click);
            // 
            // showHistogramToolStripMenuItem
            // 
            this.showHistogramToolStripMenuItem.Name = "showHistogramToolStripMenuItem";
            this.showHistogramToolStripMenuItem.Size = new System.Drawing.Size(240, 26);
            this.showHistogramToolStripMenuItem.Text = "Show Histogram";
            this.showHistogramToolStripMenuItem.Click += new System.EventHandler(this.showHistogramToolStripMenuItem_Click);
            // 
            // convulutionToolStripMenuItem
            // 
            this.convulutionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simpleBlurToolStripMenuItem,
            this.sobelOperatorsToolStripMenuItem,
            this.laplaciansToolStripMenuItem});
            this.convulutionToolStripMenuItem.Name = "convulutionToolStripMenuItem";
            this.convulutionToolStripMenuItem.Size = new System.Drawing.Size(240, 26);
            this.convulutionToolStripMenuItem.Text = "Convulution Operations";
            // 
            // simpleBlurToolStripMenuItem
            // 
            this.simpleBlurToolStripMenuItem.Name = "simpleBlurToolStripMenuItem";
            this.simpleBlurToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.simpleBlurToolStripMenuItem.Text = "Simple Blur";
            this.simpleBlurToolStripMenuItem.Click += new System.EventHandler(this.simpleBlurToolStripMenuItem_Click);
            // 
            // sobelOperatorsToolStripMenuItem
            // 
            this.sobelOperatorsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.mixed0With90ToolStripMenuItem,
            this.toolStripMenuItem3});
            this.sobelOperatorsToolStripMenuItem.Name = "sobelOperatorsToolStripMenuItem";
            this.sobelOperatorsToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.sobelOperatorsToolStripMenuItem.Text = "Sobel Operators";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(189, 26);
            this.toolStripMenuItem2.Text = "0";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // mixed0With90ToolStripMenuItem
            // 
            this.mixed0With90ToolStripMenuItem.Name = "mixed0With90ToolStripMenuItem";
            this.mixed0With90ToolStripMenuItem.Size = new System.Drawing.Size(189, 26);
            this.mixed0With90ToolStripMenuItem.Text = "Mixed 0 with 90";
            this.mixed0With90ToolStripMenuItem.Click += new System.EventHandler(this.mixed0With90ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(189, 26);
            this.toolStripMenuItem3.Text = "90";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // resetToolStripMenuItem
            // 
            this.resetToolStripMenuItem.Name = "resetToolStripMenuItem";
            this.resetToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.resetToolStripMenuItem.Text = "Reset";
            // 
            // setGreyScaleToolStripMenuItem
            // 
            this.setGreyScaleToolStripMenuItem.Name = "setGreyScaleToolStripMenuItem";
            this.setGreyScaleToolStripMenuItem.Size = new System.Drawing.Size(111, 24);
            this.setGreyScaleToolStripMenuItem.Text = "Set GreyScale";
            // 
            // laplaciansToolStripMenuItem
            // 
            this.laplaciansToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.simple141ToolStripMenuItem});
            this.laplaciansToolStripMenuItem.Name = "laplaciansToolStripMenuItem";
            this.laplaciansToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.laplaciansToolStripMenuItem.Text = "Laplacians";
            // 
            // simple141ToolStripMenuItem
            // 
            this.simple141ToolStripMenuItem.Name = "simple141ToolStripMenuItem";
            this.simple141ToolStripMenuItem.Size = new System.Drawing.Size(216, 26);
            this.simple141ToolStripMenuItem.Text = "Simple";
            this.simple141ToolStripMenuItem.Click += new System.EventHandler(this.simple141ToolStripMenuItem_Click);
            // 
            // FormImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormImage";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loadPictureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setGreyScaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem linearOperationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greyscaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalizationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nonLinearOperationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contrastEditingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem brighteningToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contrastWithNormalizationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logarithmWithNormalizationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showHistogramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem convulutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simpleBlurToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobelOperatorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem mixed0With90ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem laplaciansToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simple141ToolStripMenuItem;
    }
}

