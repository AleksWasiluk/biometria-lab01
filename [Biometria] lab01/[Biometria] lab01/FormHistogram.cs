﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _Biometria__lab01
{
    public partial class FormHistogram : Form
    {
        private DirectBitmap imageBitmap;

        public FormHistogram(DirectBitmap bitmap)
        {
            InitializeComponent();

            InitializeMyComponent(bitmap);
        }

        private void InitializeMyComponent(DirectBitmap bitmap)
        {
            imageBitmap = bitmap;

            PictureBox[] pictureBoxes = new PictureBox[3];
            pictureBoxes[0] = pictureBox1;
            pictureBoxes[1] = pictureBox2;
            pictureBoxes[2] = pictureBox3;

            var histogram = new MyHistogram(imageBitmap, pictureBoxes);

            histogram.Show();

        }
    }
}
