﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Biometria__lab01
{
    public class MyColor
    {
        private int r, g, b;

        public int R { get => r; set => r = value; }
        public int G { get => g; set => g = value; }
        public int B { get => b; set => b = value; }

        public MyColor()
        {

        }
        public MyColor(int r, int g,int b)
        {
            R = r;
            G = g;
            B = b;
        }

        public static MyColor operator+ (MyColor c1, MyColor c2)
        {
            return new MyColor(c1.R + c2.R, c1.G + c2.G, c1.B + c2.B);
        }

        public static MyColor operator +(MyColor c1, Color c2)
        {
            return new MyColor(c1.R + c2.R, c1.G + c2.G, c1.B + c2.B);
        }
    }
}
