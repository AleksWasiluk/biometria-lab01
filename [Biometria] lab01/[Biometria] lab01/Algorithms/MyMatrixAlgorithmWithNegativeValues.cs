﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Biometria__lab01
{
    public class MyMatrixAlgorithmWithNegativeValues : MyAlgorithm
    {
        public MyMatrixAlgorithmWithNegativeValues(DirectBitmap bitmap, MyMatrix m) : 
            base
            (
                bitmap,
                (DirectBitmap bmp) =>
                {
                    int dimension = m.Dimension;

                    int[,,] IntBitMap = new int[3, bmp.Width, bmp.Height];

                    MyColor maxColors = new MyColor();
                    MyColor minColors = new MyColor();

                    for (int x = 0; x < bmp.Width; x++)
                    {
                        for (int y = 0; y < bmp.Height; y++)
                        {
                            var colorX = new MyColor();
                            var colorY = new MyColor();

                            for (int w = x - dimension / 2, matrixW = 0; w <= x + dimension / 2; w++, matrixW++)
                            {

                                for (int h = y - dimension / 2, matrixH = 0; h <= y + dimension / 2; h++, matrixH++)
                                {
                                    if (bmp.HasPixel(w, h))
                                    {
                                        Color c = bmp.GetPixel(w, h);

                                        double fieldMatrixM1 = m[matrixW, matrixH];

                                        colorX.R += (int)(c.R * fieldMatrixM1);
                                        colorX.G += (int)(c.G * fieldMatrixM1);
                                        colorX.B += (int)(c.B * fieldMatrixM1);
                                    }
                                }
                            }

                            maxColors.R = Math.Max(colorX.R, maxColors.R);
                            maxColors.G = Math.Max(colorX.G, maxColors.G);
                            maxColors.B = Math.Max(colorX.B, maxColors.B);

                            minColors.R = Math.Min(colorX.R, minColors.R);
                            minColors.G = Math.Min(colorX.G, minColors.G);
                            minColors.B = Math.Min(colorX.B, minColors.B);

                            IntBitMap[0, x, y] = colorX.R;
                            IntBitMap[1, x, y] = colorX.G;
                            IntBitMap[2, x, y] = colorX.B;
                        }
                    }

                    for (int x = 0; x < bmp.Width; x++)
                    {
                        for (int y = 0; y < bmp.Height; y++)
                        {
                            double r = (IntBitMap[0, x, y] - minColors.R) * 255 / (maxColors.R - minColors.R);
                            double g = (IntBitMap[1, x, y] - minColors.G) * 255 / (maxColors.G - minColors.G);
                            double b = (IntBitMap[2, x, y] - minColors.B) * 255 / (maxColors.B - minColors.B);

                            bmp.SetPixel(x, y, Color.FromArgb((int)r, (int)g, (int)b));
                        }
                    }
                }
           )
        { }
    }
}
