﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Biometria__lab01
{
    class MyPixelAlgorithm : MyAlgorithm
    {
        public MyPixelAlgorithm(DirectBitmap Bitmap, PixelFunction pixelFunction) : 
            base(Bitmap, (bmp) =>
            {
                for(int x = 0; x < bmp.Width; x++)
                {
                    for(int y = 0; y < bmp.Height; y++)
                    {
                        pixelFunction(x, y, bmp);
                    }
                }
            })
        {

        }
    }
}
