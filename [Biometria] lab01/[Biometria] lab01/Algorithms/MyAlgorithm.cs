﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _Biometria__lab01
{
    public class MyAlgorithm
    {
        private readonly Algorithm algorithm;
        private DirectBitmap bitmap;

        public MyAlgorithm(DirectBitmap Bitmap, Algorithm Algorithm)
        {
            algorithm = Algorithm;
            bitmap = Bitmap;
        }

        public void DoAlgorithm()
        {
            algorithm(bitmap);
        }
    }
}
