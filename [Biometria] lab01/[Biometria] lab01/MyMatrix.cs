﻿using System;
using System.Collections.Generic;

namespace _Biometria__lab01
{
    public class MyMatrix
    {
        double[,] fields;

        public int Dimension { get; }
        public int Size { get; }

        public MyMatrix(int dim)
        {
            fields = new double[dim, dim];
            Dimension = dim;
            Size = dim * dim;
        }

        public MyMatrix(double [,] fields)
        {
            this.fields = fields;
            Size = fields.Length;
            Dimension = (int)Math.Sqrt(fields.Length);
        }

        public double this[int width, int height]
        {
            get
            {
                return fields[width, height];
            }
            set
            {
                fields[width, height] = value;
            }
        }
        
    }
}